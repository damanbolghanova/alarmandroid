package com.example.alarm


import android.app.*
import android.app.Notification
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TimePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    val CHANNEL_1_ID = "channel1"

    var timePicker: TimePicker? = null
    var pendingIntent: PendingIntent? = null
    var alarmManager: AlarmManager? = null
    var notificationManager: NotificationManagerCompat? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager

        notificationManager = NotificationManagerCompat.from(this)


        btn.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener {
                    timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                if (hour > 12){
                    updateText.text = SimpleDateFormat("HH:mm").format(cal.time)

                    //Toast.makeText(this, "Alarm is set to ${updateText.text}", Toast.LENGTH_SHORT).show()
                    sendNotification("Time is set to ${updateText.text}")
                }
                else{
                    updateText.text = SimpleDateFormat("HH:mm").format(cal.time)
                    //Toast.makeText(this, "Alarm is set to ${updateText.text}", Toast.LENGTH_SHORT).show()
                    sendNotification("Time is set to ${updateText.text}")
                }
                //updateText.text = SimpleDateFormat("hh:mm").format(cal.time)
            }
            val hour = cal.get(Calendar.HOUR_OF_DAY)
            val minute = cal.get(Calendar.MINUTE)
            TimePickerDialog(this, timeSetListener,
            hour, minute,
            true).show()
        }
    }

    private fun sendNotification(text: String){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel1 = NotificationChannel(
                CHANNEL_1_ID,
                "Channel 1",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel1.description = "This is channel 1"

            val manager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel1)
        }
        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_1_ID)
            .setSmallIcon(R.drawable.ic_notify)
            .setContentTitle("Alarm")
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .build()

        notificationManager?.notify(1, notification)
        }
}
